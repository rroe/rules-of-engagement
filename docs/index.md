[TOC]

# Overview

The Recruiter Rules of Engagement or RROE is a document being developed to establish
rules of engagement between
recruiters and the talent they are wishing to recruit.

It is intended to be used in a fashion similar to a software license. 

## Development

This is in very early development. Use at your own risk, a lawyer has not weighed in on this
document. Be prepared for Recruiters not engaging with you if you point to this document.

## License

This document is licensed under
[GPL V2](https://codeberg.org/rroe/rules-of-engagement/src/branch/master/LICENSE).
Feel free to fork and create your own.

## Target Audience

This document has been designed with IT professionals in mind, but can be forked
and used by anyone for their own recruiter engagements.

## How to Use

Proceed to the [releases page](https://codeberg.org/rroe/rules-of-engagement/releases) 
and download the following documents:

- pub.pem (public key used to verify files)
- rroe.txt (text version of the RROE)
- rroe.pdf (pdf version of the RROE)
- pdfdoc.sign (signature of pdf file)
- txtdoc.sign (signature of text file)

Store these files in some sort of vault.

### Share

When approached by a recruiter share this document (pdf or text), with the public key and the
signature of the file.

You can also link to the document on the repository on your Linkedin profile, with the public key
and signature

### Verify files

```
openssl dgst -sha1 -verify pub.pem -signature txtdoc.sign rroe.txt
openssl dgst -sha1 -verify pub.pem -signature pdfdoc.sign rroe.pdf
```

## Features

The document is provided with several features designed to protect all parties
involved with the recruitment procedures.

### Document Versioning

The RROE is versioned and users can pick or choose the version that best fits their situation.

### Signing

All documents a (PDF version and txt version) are digitally signed, with a provided
public key for validation.

### Email Signature (Coming soon)

Each version comes with a template to add to any email signature.

### LinkedIn Signature (Coming soon)

Each version contains a template to add to any LinkedIn profile.

### Forms (Coming soon)

The RROE defines some information a recruiter is required to provide to proceed
with the engagement. Each RROE provides a templat form that can be forwarded to recruiters.
The form does not directly relate to the candidate so it can be reused for the same position.

## Recommendations

It is highly recommended users of this document retain their own copy of this document with its
digital signature. The document is an agreement between the user and the recruiter, so 
even in the absence of the rroe website or repositories, you have a copy of the agreement sent
to the recruiter.

## Disclaimer

The document has not been vetted by any legal counsel at this time. 
As this project develops we hope to engage those with better legal understanding to
weigh in on the level at which recruiters are bound by such a document.

This is in no way an arrangement between RROE website and any other parties.  This is solely
the candidates responsibility.
