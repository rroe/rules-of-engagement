[TOC]
# The RROE Document

This page loads the sections from the RROE and is provided for easy navigation of the document.

## Reasoning

```txt
{%
include-markdown "../rroe/02-reasoning.txt"
comments=false
%}
```

## Applicability

```txt
{%
include-markdown "../rroe/03-applicability.txt"
comments=false
%}
```

## Definitions
```txt
{%
include-markdown "../rroe/10-definitions.txt"
comments=false
%}
```

## Communications

```txt
{%
include-markdown "../rroe/20-communications.txt"
comments=false
%}
```

## Position Proposals

```txt
{%
include-markdown "../rroe/30-position-proposals.txt"
comments=false
%}
```

## Data Privacy

```txt
{%
include-markdown "../rroe/40-data-privacy.txt"
comments=false
%}
```

