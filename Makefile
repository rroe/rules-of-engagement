build:
	openssl genrsa -out priv.pem 2048
	openssl rsa -in priv.pem -pubout > pub.pem
	cat rroe/* > rroe.txt
	pandoc -o rroe.pdf rroe.txt
	openssl dgst -sha1 -sign priv.pem -out txtdoc.sign rroe.txt
	openssl dgst -sha1 -verify pub.pem -signature txtdoc.sign rroe.txt
	openssl dgst -sha1 -sign priv.pem -out pdfdoc.sign rroe.pdf
	openssl dgst -sha1 -verify pub.pem -signature pdfdoc.sign rroe.pdf

clean:
	rm -f pub.pem
	rm -f priv.pem
	rm -f rroe.txt
	rm -f rroe.pdf
	rm -f pdfdoc.sign
	rm -f txtdoc.sign
